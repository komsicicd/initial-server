# Create Gitlab Service
echo "Creating Gitlab Service..."
mkdir -p /etc/gitlab
mkdir -p /var/log/gitlab
mkdir -p /var/opt/gitlab
docker service create \
    --name gitlab \
    --publish 4433:443 \
    --publish 8888:80 \
    --publish 2222:22 \
    --hostname git.komsicicd.site \
    --mount type=bind,src=/var/log/gitlab,dst=/storage/gitlab/losg \
    --mount type=bind,src=/etc/gitlab,dst=/storage/gitlab/config \
    --mount type=bind,src=/var/opt/gitlab,dst=/storage/gitlab/data \
    --network traefik-public \
    --label com.docker.stack.image=gitlab/gitlab-ce \
    --label com.docker.stack.namespace=gitlab \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.gitlab.rule=Host \
    --label traefik.http.routers.gitlab.tls=true \
    --label traefik.http.routers.gitlab.tls.certresolver=mytlschallenge \
    --label traefik.http.services.gitlab.loadbalancer.server.port=80 \
    gitlab/gitlab-ce
