# Create Jenkins Service
echo "Creating Jenkins Service..."
mkdir -p /var/jenkins_home
chmod -R 777 /var/jenkins_home
docker service create \
    --name jenkins \
    --publish 9999:8080 \
    --publish 50000:50000 \
    --mount type=bind,src=/var/jenkins_home,dst=/var/jenkins_home \
    --mount type=bind,src=/usr/bin/docker,dst=/usr/bin/docker \
    --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    --network traefik-public \
    --label com.docker.stack.image=jenkins/jenkins \
    --label com.docker.stack.namespace=jenkins \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.jenkins.rule=Host \
    --label traefik.http.routers.jenkins.tls=true \
    --label traefik.http.routers.jenkins.tls.certresolver=mytlschallenge \
    --label traefik.http.services.jenkins.loadbalancer.server.port=8080 \
    jenkins/jenkins
