# Create Approval-Integrator Service
echo "Creating Approval-Integrator Service..."
docker service create \
    --name approval-integrator \
    --publish 3536:3536 \
    --network traefik-public \
    --label com.docker.stack.image=regis.komsicicd.site/approval-integrator \
    --label com.docker.stack.namespace=approval-integrator \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.approval-integrator.rule=Host \
    --label traefik.http.routers.approval-integrator.tls=true \
    --label traefik.http.routers.approval-integrator.tls.certresolver=mytlschallenge \
    --label traefik.http.services.approval-integrator.loadbalancer.server.port=3536 \
    regis.komsicicd.site/approval-integrator

