# Create Mattermost Services
echo "Creating Mattermost Services..."
# https://github.com/mattermost/mattermost-docker.git
mkdir -p /var/lib/mattermost/{cert,config,data,logs,plugins}
chmod 777 /var/lib/mattermost/{cert,config,data,logs,plugins}
chown 2000:2000 /var/lib/mattermost/{cert,config,data,logs,plugins}
docker stack deploy -c mattermost-docker/docker-stack.yml mattermost
docker service update \
    --network-add traefik-public \
    --label-add com.docker.stack.image=mattermost/mattermost-prod-app \
    --label-add com.docker.stack.namespace=mattermost \
    --label-add traefik.docker.network=traefik-public \
    --label-add traefik.enable=true \
    --label-add traefik.http.routers.mattermost.rule=Host \
    --label-add traefik.http.routers.mattermost.tls=true \
    --label-add traefik.http.routers.mattermost.tls.certresolver=mytlschallenge \
    --label-add traefik.http.services.mattermost_web.loadbalancer.server.port=80 \
    mattermost_web
