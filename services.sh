# Create Portainer Service
sh portainer-service.sh

# Create MySQL Service
sh mysql-service.sh

# Create Jenkins Service
sh jenkins-service.sh

# Create SonarQube Service
sh sonarqube-service.sh

# Create Gitlab Service
sh gitlab-service.sh

# Create Mattermost Services
sh mattermost-service.sh
