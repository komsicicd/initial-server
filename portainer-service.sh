# Create Portainer Service
echo "Creating Portainer Service..."
mkdir -p /host/data
docker service create \
    --name portainer \
    --publish 9000:9000 \
    --constraint 'node.role == manager' \
    --mount type=bind,src=/host/data,dst=/data \
    --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    --network traefik-public \
    --label com.docker.stack.image=portainer/portainer \
    --label com.docker.stack.namespace=portainer \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.portainer.rule=Host \
    --label traefik.http.routers.portainer.tls=true \
    --label traefik.http.routers.portainer.tls.certresolver=mytlschallenge \
    --label traefik.http.services.portainer.loadbalancer.server.port=9000 \
    portainer/portainer \
    -H unix:///var/run/docker.sock
