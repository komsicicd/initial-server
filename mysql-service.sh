# Create MySQL Service
echo "Creating MySQL Service..."
mkdir -p /var/lib/mysql/mysql_data
mkdir -p /etc/mysql/mysql_config
docker service create \
    --name mysql \
    --env MYSQL_ROOT_PASSWORD=komsicicd \
    --publish 6033:3306 \
    --mount type=bind,src=/var/lib/mysql/mysql_data,dst=/var/lib/mysql \
    --mount type=bind,src=/etc/mysql/mysql_config,dst=/etc/mysql \
    --network traefik-public \
    --label com.docker.stack.image=mysql:5.7 \
    --label com.docker.stack.namespace=mysql \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.mysql.rule=Host \
    --label traefik.http.routers.mysql.tls=true \
    --label traefik.http.routers.mysql.tls.certresolver=mytlschallenge \
    --label traefik.http.services.mysql.loadbalancer.server.port=3306 \
    mysql:5.7
