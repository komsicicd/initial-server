# Create SonarMatt Service
echo "Creating SonarMatt Service..."
docker service create \
    --name sonarmatt \
    --publish 8090:8090 \
    --network traefik-public \
    --label com.docker.stack.image=regis.komsicicd.site/sonarmatt \
    --label com.docker.stack.namespace=sonarmatt \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.sonarmatt.rule=Host \
    --label traefik.http.routers.sonarmatt.tls=true \
    --label traefik.http.routers.sonarmatt.tls.certresolver=mytlschallenge \
    --label traefik.http.services.sonarmatt.loadbalancer.server.port=8090 \
    regis.komsicicd.site/sonarmatt
