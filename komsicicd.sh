# Update Dependencies
echo "Update Dependencies..."
apt update

# Intall git
echo "Installing git..."
apt install git --yes

# Install Docker
echo "Installing Docker..."
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
chmod -R 777 /var/run/docker.sock

# Initialize Docker Swarm Cluster
echo "Initialize Docker Swarm Cluster..."
docker swarm init

# Create Traefik & Consul Services
echo "Creating Traefik & Consul Services..."
docker network create --driver=overlay traefik-public
docker stack deploy -c traefik.yml traefik

# Create Portainer Service
sh portainer-service.sh

# Create MySQL Service
sh mysql-service.sh

# Create Jenkins Service
sh jenkins-service.sh

# Create SonarQube Service
sh sonarqube-service.sh

# Create Gitlab Service
sh gitlab-service.sh

# Create Mattermost Services
sh mattermost-service.sh

echo "Done ..."
