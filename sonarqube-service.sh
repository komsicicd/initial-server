# Create SonarQube Service
echo "Creating SonarQube Service..."
mkdir -p /opt/sonarqube/{conf,data,logs,extensions}
chmod 777 /opt/sonarqube/{conf,data,logs,extensions}
chown 2000:2000 /opt/sonarqube/{conf,data,logs,extensions}
docker service create \
    --name sonarqube \
    --publish 9009:9000 \
    --mount type=bind,src=/opt/sonarqube/conf,dst=/opt/sonarqube/conf \
    --mount type=bind,src=/opt/sonarqube/data,dst=/opt/sonarqube/data \
    --mount type=bind,src=/opt/sonarqube/logs,dst=/opt/sonarqube/logs \
    --mount type=bind,src=/opt/sonarqube/extensions,dst=/opt/sonarqube/extensions \
    --network traefik-public \
    --label com.docker.stack.image=sonarqube \
    --label com.docker.stack.namespace=sonarqube \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.sonarqube.rule=Host \
    --label traefik.http.routers.sonarqube.tls=true \
    --label traefik.http.routers.sonarqube.tls.certresolver=mytlschallenge \
    --label traefik.http.services.sonarqube.loadbalancer.server.port=9000 \
    sonarqube
