# Create Registry Service
echo "Creating Registry Service..."
docker service create \
    --name registry \
    --publish 5000:5000 \
    --network traefik-public \
    --label com.docker.stack.image=registry:2 \
    --label com.docker.stack.namespace=registry \
    --label traefik.docker.network=traefik-public \
    --label traefik.enable=true \
    --label traefik.http.routers.registry.rule=Host \
    --label traefik.http.routers.registry.tls=true \
    --label traefik.http.routers.registry.tls.certresolver=mytlschallenge \
    --label traefik.http.services.registry.loadbalancer.server.port=5000 \
    registry:2
docker stack deploy -c registryui-service.yml registry-ui
